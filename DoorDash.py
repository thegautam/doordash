import argparse
from enum import Enum
import os
import math
import pickle
from collections import namedtuple

import lightgbm as lgb
import numpy as np
import pandas as pd
from IPython import embed
from sklearn import metrics, pipeline
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.tree import DecisionTreeRegressor

adjusted_label = 'y_adj'
true_label = 'y_true'
predicted_label = 'y_pred'
train_split = 0.70

class DoorDashModel:

    ''' Basic features to start wtih. '''
    features = [
        'market_id',
        'max_item_price',
        'min_item_price',
        'num_distinct_items',
        'order_protocol',
        'subtotal',
        'total_busy_dashers',
        'total_items',
        'total_onshift_dashers',
        'total_outstanding_orders',
    ]

    def get_training_data(self, training_datafile):
        '''
        Read the dataset, parse date fields, add labels & features.
        '''
        data = DataUtils().read_training_csv(training_datafile)
        data = data.dropna()
        data = self.add_label(data)
        data = data[(data[adjusted_label] > 0) & (data[adjusted_label] < 2 * 3600)]
        data = self.add_features(data)
        log('Read {} lines'.format(len(data)))
        return data

    def add_features(self, data):
        ''' Add features derived from the data '''
        new_features_1 = {
            'created_weekday': data.created_at.dt.weekday,
            'created_time': data.created_at.dt.hour*100 + data.created_at.dt.minute,
            'created_quarter': data.created_at.dt.hour*100 + data.created_at.dt.minute//15,

            'total_available_dashers': data.total_onshift_dashers - data.total_busy_dashers,
        }

        data = self._add_features(data, new_features_1)

        # Level 2 features that are dependant on level 1.
        new_features_2 = {
            'outstanding_per_dasher': data.total_outstanding_orders/data.total_available_dashers,
        }

        data = self._add_features(data, new_features_2)

        return data

    def _add_features(self, data, new_features):
        ''' 
        Private function to actually add features.
        This checks whether features actually exist before adding them.
        '''
        for f_name, f_value in new_features.items():
            if f_name not in self.features:
                log('Adding {} to features'.format(f_name))
                self.features.append(f_name)
            if f_name not in data.columns:
                log('Adding {} to columns'.format(f_name))
                data[f_name] = f_value

        return data

    def sweep_lgb(self, data, features):
        param_grid = {
            'num_leaves': [10, 20, 30, 40, 50, 75, 100, 200, 500],
            'learning_rate': [0.01, 0.02, 0.03, 0.05, 0.1],
            'n_estimators': [100, 200, 500, 1000, 2000, 3000, 5000]
        }

        model = lgb.LGBMRegressor(objective='regression')

        grid_search = RandomizedSearchCV(estimator = model, param_distributions = param_grid, 
                                         n_iter = 20, cv = 5, n_jobs = -1, verbose = 2)
        grid_search.fit(data[features], data[adjusted_label],)

        print(grid_search.cv_results_)
        print(grid_search.best_estimator_)
        print(grid_search.best_score_)
        print(grid_search.best_params_)

    def train_lgb(self, data, features, validate):
        gbm = lgb.LGBMRegressor(objective='regression',
                                num_leaves=30,
                                learning_rate=0.02,
                                n_estimators=2000,
                                silent=True)

        if validate:
            # Split the training data & validate with early stopping
            data_utils = DataUtils()
            training_data, validation_data = data_utils.split_data(data, train_split)
            gbm.fit(training_data[features], training_data[adjusted_label],
                eval_set=[(validation_data[features], validation_data[adjusted_label])],
                eval_metric='l2',
                early_stopping_rounds=100
            )
        else:
            # Train on the entire dataset
            training_data = data
            gbm.fit(training_data[features], training_data[adjusted_label],
                eval_metric='l2',
            )

        return gbm

    def add_counting_features(self, training_data, validation_data):
        counting_columns = ['store_id', 'store_primary_category', 'created_weekday',
                            'created_quarter',]
        for col in counting_columns:
            counting_features, counting_data, default = self.learn_counting_features(
                data=training_data, column=col)
            self.features.extend(counting_features)
            training_data = self.merge_counting_features(
                data=training_data, column=col, features=counting_features, 
                counting_data=counting_data, default=default)

            if validation_data is not None:
                validation_data = self.merge_counting_features(
                    data=validation_data, column=col, features=counting_features, 
                    counting_data=counting_data, default=default)

        return training_data, validation_data

    def learn_counting_features(self, data, column):
        aggregators = [np.mean, np.max, np.min, np.size]
        names = [agg.__name__ for agg in aggregators]
        features = ['_'.join([name, adjusted_label, column]) for name in names]
        
        counting_data = data.groupby(column, as_index=False)[adjusted_label] \
                            .agg(aggregators).reset_index() \
                            .rename(columns=dict(zip(names, features)))
        
        return features, counting_data, data[adjusted_label].mean()

    def merge_counting_features(self, data, column, features, counting_data, default):
        data = data.merge(counting_data, on=column, how='left')
        for f in features:
            data[f].fillna(data[f].mean(), inplace=True)
        
        return data

    Metrics = namedtuple('Metrics', ['MAE', 'RMSE', 'P600', 'MRE'])
    def evaluate(self, y_true, y_pred):
        mae = metrics.mean_absolute_error(y_true, y_pred)
        rmse = math.sqrt(metrics.mean_squared_error(y_true, y_pred))
        y_diff = y_pred-y_true
        p600 = len(y_diff[y_diff>600])/len(y_diff)
        mre = (abs(y_pred-y_true)/y_pred).mean()
        return self.Metrics(mae, rmse, p600, mre)

    def add_label(self, data):
        '''
        Simplifying the label by focusing on time store takes to make
        the order and dasher takes to pick up the order.

        This simplication should be especially helpful for 
        tree based models. We have add these columns back when making
        final test predictions.
        '''

        data[true_label] = (data.actual_delivery_time - data.created_at).apply(pd.Timedelta.total_seconds)
        data[adjusted_label] = self._adjust_label(data[true_label], data, is_training=True)

        return data

    def _adjust_label(self, label, data, is_training):
        adjustment = data.estimated_order_place_duration \
                     + data.estimated_store_to_consumer_driving_duration

        if is_training:
            adjustment = -1 * adjustment

        return label + adjustment

    def save(self, trained_model, model_filepath):
        log("Saving model to {}".format(model_filepath))
        with open(model_filepath, 'wb') as model_file:
            pickle.dump(trained_model, model_file)

    def load(self, filepath):
        log('Loading model from {} ...'.format(filepath))
        assert os.path.exists(filepath), 'Model file missing'
        with open(filepath, 'rb') as modelf:
            self.model = pickle.load(modelf)
        log('Done')

    def predict(self, input_data):
        data = input_data[self.features]
        y_pred = self.model.predict(data)
        y_pred = self._adjust_label(y_pred, input_data, is_training=False)
        return y_pred

class DataUtils():
    ''' Helper functions for data pipelines '''

    def read_training_csv(self, filepath):
        assert os.path.exists(filepath)
        log('Reading training data {} ...'.format(filepath))
        date_columns = ['created_at', 'actual_delivery_time']
        data = pd.read_csv(filepath, parse_dates=date_columns)
        return data


    def read_json(self, filepath, training_data):
        assert os.path.exists(filepath)
        log('Reading prediction data from {}'.format(filepath))
        
        data = pd.read_json(filepath, lines=True)
        data = data.replace({'NA':None})

        # Make prediction datatypes line up with training datatypes.
        # We can do this on a column by column basis if training
        # data isn't handy while serving.
        types = {}
        for k, v in dict(training_data.dtypes).items():
            if k in data.columns:
                types[k] = v
        data = data.astype(types)
        
        log('Read {} lines'.format(len(data)))

        return data

    
    def split_data(self, data, ratio):
        ''' 
        Split the data by time so we can validate like
        we would do in production i.e. on future orders
        '''
        total_size = data.shape[0]
        training_size = int(ratio * total_size)

        data = data.sort_values('created_at')
        training_data = data.iloc[:training_size]
        validation_data = data.iloc[training_size:]

        assert training_data.created_at.max() <= validation_data.created_at.min()
        
        return training_data, validation_data

def log(message):
    print(message)

def train(args):
    ''' Run the prediction pipeline & variants '''

    # Read the training data
    model = DoorDashModel()
    data_utils = DataUtils()
    data = model.get_training_data(args.training_csv)

    # In full training mode, train the best model on full data.
    if args.run == RunMode.train:
        # Add counting features & learn on full data
        data, _ = model.add_counting_features(data, None)
        trained_model = model.train_lgb(data, model.features, validate=False)

        # Print feature importances
        fi = zip(model.features, trained_model.feature_importances_)
        print(sorted(list(fi), key=lambda x:x[1], reverse=True))

        # Save model
        model.save(trained_model, args.model)

    # Split the data by time in validation mode so we can test on future
    elif args.run == RunMode.train_validate:
        # Split into a validation set
        training_data, validation_data = data_utils.split_data(data, train_split)

        # Write out the validation data so we can test it later
        validation_data.to_json('validation_data.json', orient='records', lines=True)

        # Add counting features                                
        training_data, validation_data = model.add_counting_features(
                                            training_data, validation_data)    

        # Train model & make predictions
        trained_model = model.train_lgb(training_data, model.features, validate=True)
        y_pred = trained_model.predict(validation_data[model.features])

        # Evaluate
        metrics = model.evaluate(validation_data[adjusted_label], y_pred)
        print(metrics)

        # Print feature importances
        fi = zip(model.features, trained_model.feature_importances_)
        print(sorted(list(fi), key=lambda x:x[1], reverse=True))

        # Save model
        model.save(trained_model, args.model)

    # Sweep on k-fold cross-validation
    elif args.run == RunMode.train_sweep:
        data, _ = model.add_counting_features(data, None)
        model.sweep_lgb(data, model.features)
    


def predict(args):
    ''' Run the prediction pipeline & variants '''

    # Load up the saved model file.
    model = DoorDashModel()
    model.load(args.model)

    # Read the training data to get types and re-compute counting
    # features. These could be saved while training too but ideally
    # we'd like to update them in real-time with some decay so we
    # can capture things like how busy is a restaurant in last hour.
    data_utils = DataUtils()
    training_data = model.get_training_data(args.training_csv)
    if args.run == RunMode.predict_test:
        # This forces to learn counting features on original data.
        training_data, _ = data_utils.split_data(training_data, train_split)    
    data = data_utils.read_json(args.prediction_json, training_data)
    data = model.add_features(data)
    _, data = model.add_counting_features(training_data, data)

    # Make actual predictions. I'm doing predictions on the entire batch.
    # In production, this will likely be on a single rows unless our RPS & 
    # latencies also allow us to batch live requests.
    data[predicted_label] = model.predict(data)

    if args.run == RunMode.predict_test:
        # In test mode, we have true observed labels and we can evaluate the
        # model to check if we got similar results as training. Ideally, I
        # would like to save the validation results while training and actually
        # assert on them here.
        data = model.add_label(data)
        print(model.evaluate(data[true_label], data[predicted_label]))
        data[[true_label, predicted_label]].to_csv(args.output)
    else:
        # This is final predictions file.
        data[['delivery_id', predicted_label]].to_csv(args.output, index=False)

def main(args):
    if args.run in [RunMode.train, RunMode.train_sweep, RunMode.train_validate]:
        train(args)
    elif args.run in [RunMode.predict, RunMode.predict_test]:
        predict(args)

class RunMode(Enum):
    train = 'train'
    train_validate = 'train_validate'
    train_sweep = 'train_sweep'
    predict = 'predict'
    predict_test = 'predict_test'

    def __str__(self):
        return self.value

def get_args():
    ''' Parser for cli '''
    parser = argparse.ArgumentParser(description='Predict delivery time.')
    parser.add_argument('-m', '--model', required=True, help='the model file')
    parser.add_argument('-t', '--training_csv', required=True, help='input json to make predictions')
    parser.add_argument('-p', '--prediction_json', help='input json to make predictions')
    parser.add_argument('-o', '--output', help='output tsv with delivery times')
    parser.add_argument('-r', '--run', required=True, type=RunMode, choices=list(RunMode), help='Run in training, prediction, or test mode.')

    return parser

if __name__ == '__main__':
    main(get_args().parse_args())
