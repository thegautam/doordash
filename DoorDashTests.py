''' Tests for the DoorDash module '''

'''
I ran out of time for writing unit tests but was able to get an E2E test in.
This can be done by running first in train_validate mode and then in 
predict_test mode. The MAE & RMSE nubmers should match up for those.
'''

'''
from DoorDash import *

def test_model_predict():
    model = LgbModel('model.pkl')
    assert model, 'Model did not load'
    assert model.predict == 42

def test_get_args():
    ''' Test if cli is setup correctly. '''
    modelfile = 'model88'
    inputfile = 'json38'
    outputfile = 'out32'
    args_list = ['-m', modelfile, '-i', inputfile, '-o', outputfile]
    args = get_args().parse_args(args_list)
    assert args.model == modelfile
    assert args.input == inputfile
    assert args.output == outputfile
'''