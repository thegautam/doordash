This folder contains the solution to delivery time prediction task.

The final output file is: predictions.tsv

There's a requirements.txt file describing the dependencies. You'll have to install
LightGBM from here: http://lightgbm.readthedocs.io/en/latest/

To train the model on full data run:

> python3 DoorDash.py -m model.pkl -t historical_data.csv -r train

To train the model on full data run:

> python3 DoorDash.py -m model.pkl -t historical_data.csv -r train

To predict delivery times run:

> python3 DoorDash.py -m model.pkl -t historical_data.csv  -p data_to_predict.json  -o predictions.tsv -r predict

There's a special test mode that uses a validation file created during training.
This can be used to ensure that RMSE on validation set during training matches
on same validation set during predict mode.

> python3 DoorDash.py -m model.pkl -t historical_data.csv  -p validation_data.json  -o predictions.tsv -r predict_test
