lightgbm==2.1.1
pandas==0.22.0
numpy==1.14.0
ipython==6.4.0
scikit_learn==0.19.1
train==0.0.3
